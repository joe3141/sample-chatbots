# sample-chatbots

NodeJS chat-bots using Microsoft&#39;s bot framework. 
greeting.js greets the user after asking for their name.
restaurant.js reserves a table or orders dinner from a restaurant.
App.js is the main driver that executes any dialog and handles all connection.