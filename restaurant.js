exports.bot = function (builder, connector, storage) {
	
	var bot = new builder.UniversalBot(connector, function(session){
	    var msg = "Welcome to the reservation bot. Please say `Dinner Reservation` or `Order Dinner`";
	    session.send(msg);
	}).set('storage', storage);

	bot.dialog('dinnerReservation', [
	    function (session) {
	        session.send("Welcome to the dinner reservation.");
	        session.beginDialog('askForDateTime');
	    },
	    function (session, results) {
	        session.dialogData.reservationDate = builder.EntityRecognizer.resolveTime([results.response]);
	        session.beginDialog('askForPartySize');
	    },
	    function (session, results) {
	        session.dialogData.partySize = results.response;
	        session.beginDialog('askForReserverName');
	    },
	    function (session, results) {
	        session.dialogData.reservationName = results.response;

	        // Process request and display reservation details
	        session.send(`Reservation confirmed. Reservation details: <br/>Date/Time: ${session.dialogData.reservationDate} <br/>Party size: ${session.dialogData.partySize} <br/>Reservation name: ${session.dialogData.reservationName}`);
	        session.endDialog();
	    }
	])
	.triggerAction({
	    matches: /^dinner reservation$/i,
	    confirmPrompt: "This will cancel your current request. Are you sure?"
	})
	.endConversationAction("endOrderDinner", "Ok. Goodbye.",
	    {
	        matches: /^cancel$|^goodbye$/i,
	        confirmPrompt: "This will cancel your order. Are you sure?"
	    }
	);
	
	bot.dialog('askForDateTime', [
	    function (session) {
	        builder.Prompts.time(session, "Please provide a reservation date and time (e.g.: June 6th at 5pm)");
	    },
	    function (session, results) {
	        session.endDialogWithResult(results);
	    }
	]);

	bot.dialog('askForPartySize', [
	    function (session) {
	        builder.Prompts.text(session, "How many people are in your party?");
	    },
	    function (session, results) {
	        session.endDialogWithResult(results);
	    }
	]).beginDialogAction('partySizeHelpAction', 'partySizeHelp', { matches: /^help$/i });

	bot.dialog('partySizeHelp', function(session, args, next) {
	    var msg = "Party size help: Our restaurant can support party sizes up to 150 members.";
	    session.endDialog(msg);
	});

	bot.dialog('askForReserverName', [
	    function (session) {
	        builder.Prompts.text(session, "Who's name will this reservation be under?");
	    },
	    function (session, results) {
	        session.endDialogWithResult(results);
	    }
	]);


	var dinnerMenu = {
	    "Potato Salad - $5.99": {
	        Description: "Potato Salad",
	        Price: 5.99
	    },
	    "Tuna Sandwich - $6.89": {
	        Description: "Tuna Sandwich",
	        Price: 6.89
	    },
	    "Clam Chowder - $4.50":{
	        Description: "Clam Chowder",
	        Price: 4.50
	    }
	};

	bot.dialog('orderDinner', [
	    function(session){
	        session.send("Lets order some dinner!");
	        builder.Prompts.choice(session, "Dinner menu:", dinnerMenu, { listStyle: 3 });
	    },
	    function (session, results) {
	        if (results.response) {
	            var order = dinnerMenu[results.response.entity];
	            var msg = `You ordered: ${order.Description} for a total of $${order.Price}.`;
	            session.dialogData.order = order;
	            session.send(msg);
	            builder.Prompts.text(session, "What is your table number?");
	        } 
	    },
	    function(session, results){
	        if(results.response){
	            session.dialogData.table = results.response;
	            var msg = `Thank you. Your order will be delivered to table #${session.dialogData.table}`;
	            // session.endDialog(msg);
	            session.endConversation(msg);
	        }
	    }
	])
	.triggerAction({
	    matches: /^order dinner$/i,
	    confirmPrompt: "This will cancel your order. Are you sure?"
	})
	.endConversationAction("endOrderDinner", "Ok. Goodbye.",
	    {
	        matches: /^cancel$|^goodbye$/i,
	        confirmPrompt: "This will cancel your order. Are you sure?"
	    }
	);

	return bot;
}