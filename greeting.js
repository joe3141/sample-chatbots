exports.bot = function (builder, connector, storage) {
	var bot = new builder.UniversalBot(connector, function (session) {
	session.beginDialog("greetings");
	}).set('storage', storage);


	bot.dialog("greetings", [
		function (session) {
			builder.Prompts.text(session, "Hi, what's your name?");
		},

		function (session, results) {
			session.endDialog(`Hello ${results.response}!`);
		}
	]);

	return bot;
}