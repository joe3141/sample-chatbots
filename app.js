var restify = require('restify');
var builder = require('botbuilder');

var greeting = require('./greeting');
var restaurant = require('./restaurant');

var server = restify.createServer();
server.listen(process.env.port || process.env.PORT || 3978, function () {
	console.log('%s Listening to %s', server.name, server.url);
});

var inMemoryStorage = new builder.MemoryBotStorage();

var connector = new builder.ChatConnector({
	appId: process.env.MicrosoftAppId,
	appPassword: process.env.MicrosoftAppPassword
});

server.post('/api/messages', connector.listen());


greeting.bot(builder, connector, inMemoryStorage);

restaurant.bot(builder, connector, inMemoryStorage);

